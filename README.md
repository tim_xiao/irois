# irOis

Overnight index swaps (OIS) curves became the market standard for discounting collateralized cashflows. The reason often given for using the OIS rate as the discount rate is that it is derived from the fed funds rate and the fed funds rate is the int